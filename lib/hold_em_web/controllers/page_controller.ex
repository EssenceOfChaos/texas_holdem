defmodule HoldEmWeb.PageController do
  use HoldEmWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
