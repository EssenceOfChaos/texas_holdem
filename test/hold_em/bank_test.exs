defmodule HoldEm.BankTest do
  use ExUnit.Case, async: false

  test "deposits and withdrawals" do
    HoldEm.Bank.start_link

    HoldEm.Bank.deposit(:player_one, 100)

    assert HoldEm.Bank.withdraw(:player_one, 75) == :ok
    assert HoldEm.Bank.balance(:player_one) == 25
    assert HoldEm.Bank.withdraw(:player_one, 75) == {:error, %{reason: :insufficient_funds}}
    assert HoldEm.Bank.withdraw(:player_one, -75) == :error

    assert HoldEm.Bank.withdraw(:player_two, 35) == {:error, %{reason: :insufficient_funds}}
    assert HoldEm.Bank.balance(:player_two) == 0

    HoldEm.Bank.deposit(:player_one, -100)
    assert HoldEm.Bank.balance(:player_one) == 25
    assert HoldEm.Bank.withdraw(:player_one, 25) == :ok
    assert HoldEm.Bank.balance(:player_one) == 0
  end
end