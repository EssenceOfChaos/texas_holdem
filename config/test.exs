use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :hold_em, HoldEmWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :hold_em, HoldEm.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "hold_em_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
